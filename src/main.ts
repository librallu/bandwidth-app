/*
  Entry point of the main program.
  It only declares the component App
*/
import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
