var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require("html-webpack-plugin")
var glob = require('glob')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var merge = require("webpack-merge");
var ImageminPlugin = require('imagemin-webpack-plugin').default
const CompressionPlugin = require("compression-webpack-plugin")


let commonConfig = {
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, './public/'),
    filename: 'build.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name][hash].[ext]'
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: "file-loader",
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Bandwidth App',
      template: 'src/index.html'
    })
  ]
}



let devConfig = {
  module:{
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader'
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader", options: { sourceMap: true } },
          { loader: "sass-loader", options: { sourceMap: true } }
        ]
      }
    ]
  },
  watchOptions: {
    poll: true
  },
  devServer: {
    inline: true
  },
  devtool: '#eval-source-map'
}



let prodConfig = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': ExtractTextPlugin.extract({
              fallback: "vue-style-loader", use:["css-loader", "sass-loader"]
            }),
            'css': ExtractTextPlugin.extract({
              fallback: "vue-style-loader", use:["css-loader"]
            }),
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use:["css-loader"]})
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use:["css-loader", "sass-loader"]})
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("styles.css"),
    new ImageminPlugin({
      pngquant: {
        quality: '95-100'
      },
      jpegtran: {
        progressive: true
      }
    }),
    new CompressionPlugin({
      test: /\.js/
    })
  ]
}



module.exports = mode => {
  if ( mode === "production" ) {
    return merge(commonConfig, prodConfig)
  } else {
    return merge(commonConfig, devConfig)
  }
}
